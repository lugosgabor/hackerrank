package hackerrank.ctci

object ArrayLeftRotation {

  def solve(array: Array[Int], position: Int): Array[Int] = {
    array match {
      case Array() => Array()
      case _ =>
        val (firstHalf, secondHalf) = array.splitAt(position % array.length)
        secondHalf ++ firstHalf
    }
  }
}
