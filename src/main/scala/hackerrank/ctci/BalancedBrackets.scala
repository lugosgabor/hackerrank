package hackerrank.ctci

import scala.annotation.tailrec
import scala.util.matching.Regex

object BalancedBrackets {

  def solve(expression: String): String = {
    val Opening: Regex = "([\\(\\{\\[])".r
    val Closing: Regex = "([\\)\\}\\]])".r
    val pair: Map[Char, Char] = Map(')' -> '(', ']' -> '[', '}' -> '{', '(' -> ')', '[' -> ']', '{' -> '}')

    @tailrec
    def process(expression: List[Char], stack: List[Char] = List.empty): String = {
      (expression, stack) match {
        case (Nil, Nil) => "YES"
        case (Opening(next) :: remaining, _) => process(remaining, next :: stack)
        case (Closing(next) :: remaining, previous :: preceding) if next == pair(previous) => process(remaining, preceding)
        case _ => "NO"
      }
    }

    process(expression.toList)
  }
}
