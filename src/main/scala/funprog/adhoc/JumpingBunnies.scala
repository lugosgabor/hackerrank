package funprog.adhoc

import scala.annotation.tailrec

object JumpingBunnies {

  def solve(ns: List[Long]): Long = {
    @tailrec
    def gcd(a: Long, b: Long): Long = if (b == 0) a else gcd(b, a % b)

    def lcm(a: Long, b: Long): Long = a / gcd(a, b) * b

    ns.foldLeft(1L)((ans, a) => lcm(ans, a))
  }
}
