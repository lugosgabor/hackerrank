package hackerrank.ctci

import hackerrank.ctci.ArrayLeftRotation.solve
import org.scalatest.{FunSuite, Matchers}

class ArrayLeftRotationSuite extends FunSuite with Matchers {

  test("solve() should be identical when rotating empty array") {
    solve(Array(), 3) shouldBe Array()
  }

  test("solve() should be identical when rotating array with 0 position") {
    solve(Array.range(1, 5), 0) shouldBe Array.range(1, 5)
  }

  test("solve() should be identical when rotating array with array.length position") {
    solve(Array.range(1, 5), 5) shouldBe Array.range(1, 5)
  }

  test("solve() should rotate custom array") {
    solve(Array(1, 2, 3, 4, 5), 4) shouldBe Array(5, 1, 2, 3, 4)
  }

}
