package hackerrank.ctci

import BalancedBrackets.solve
import org.scalatest.{FunSuite, Matchers}

class BalancedBracketsSuite extends FunSuite with Matchers {

  test("solve() should accept empty string as balanced expression") {
    solve("") shouldBe "YES"
  }

  test("solve() should accept a balanced expression") {
    solve("{{[[(())]]}}") shouldBe "YES"
  }

  test("solve() should refuse expression with unclosed bracket") {
    solve("([)") shouldBe "NO"
  }

  test("solve() should refuse expression with overlapping brackets") {
    solve("{[(])}") shouldBe "NO"
  }

  test("solve() should refuse expression with unexpected closing bracket") {
    solve("{}]") shouldBe "NO"
  }
}
